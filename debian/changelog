libcommons-el-java (1.0-10) unstable; urgency=medium

  * Install the Maven artifacts
  * Depend on libservlet-api-java instead of libjsp-api-java
  * Build with the DH sequencer instead of CDBS
  * Removed the javadoc
  * Removed Niels Thykier from the uploaders (Closes: #770571)
  * Removed 0005-increase-target-and-source-version-to-1.5.patch
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13
  * Updated the watch file
  * Use DEB-3 patch headers
  * Use a secure URL for the homepage

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 26 Jan 2021 16:41:20 +0100

libcommons-el-java (1.0-9) unstable; urgency=medium

  * Transition to the Servlet API 3.1
  * Standards-Version updated to 3.9.8 (no changes)
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 03 Jun 2016 16:41:42 +0200

libcommons-el-java (1.0-8) unstable; urgency=low

  * debian/control:
    - Updated Standards-Version to 3.9.4 (no changes)
    - Removed Michael Koch from the uploaders (Closes: #654054)
    - Use canonical URLs for the Vcs-* fields
  * Transition to libservlet3.0-java

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 10 Sep 2013 11:13:19 +0200

libcommons-el-java (1.0-7) unstable; urgency=low

  * Team upload.
  * Change Build-Depend-Indep: libservlet2.5-java.
  * Refresh patches and update OSGi metadata.
  * Remove Arnaud from Uploaders list.
  * Remove Java runtime from Depends.
  * Update Standards-Version: 3.9.1.
  * Fix the name of the doc-base Document.

 -- Torsten Werner <twerner@debian.org>  Fri, 02 Sep 2011 22:38:06 +0200

libcommons-el-java (1.0-6) unstable; urgency=low

  * Set target version to 1.5. (Closes: #559323)
  * Bumped Standards-Version to 3.8.4 - no changes required.

 -- Niels Thykier <niels@thykier.net>  Fri, 12 Feb 2010 20:00:14 +0100

libcommons-el-java (1.0-5) unstable; urgency=low

  [ Emmanuel Bourg ]
  * Update of the URLs
  * Rename Jakarta Commons to Apache Commons

  [ Niels Thykier ]
  * Added myself to uploaders.
  * Updated Build-Depends(-Indep).
    - Replace java-gcj-compat with default-jdk
    - Move default-jdk and ant to Build-Depends.
  * Made debian/rules respect errors.
  * Bumped debhelper compat to 7.
  * Bumped Standards-Version to 3.8.3:
    - Changed section to java.
    - Added Vcs-* fields.
  * Added more alternatives to java and changed to the -headless
    version of them.
  * Added missing ${misc:Depends}.
  * Imported patch from Ubuntu to fix FTBFS.
    - Thanks to Ilya Barygin for the patch.
  * Updated existing patches to be "quilt" appliable.
  * Imported patch from Fedora to add OSGi metadata. (Closes: #558180)
  * Converted source format to 3.0 (quilt).
  * Added descriptions to the patches.
  * Added doc-base for the javadoc.

 -- Niels Thykier <niels@thykier.net>  Thu, 26 Nov 2009 23:53:34 +0100

libcommons-el-java (1.0-4) unstable; urgency=low

  * Use java-gcj-compat intead of kaffe. Closes: #399372.
  * Moved debhelper and cdbs to Build-Depends.
  * Use Homepage: field in debian/control instead of homepage url in long
    description.
  * Updated debhelper level to 5.
  * Removed Wolfgang and added myself to Uploaders.
  * Updated Standards-Version to 3.7.2.

 -- Michael Koch <konqueror@gmx.de>  Sun, 07 Oct 2007 13:09:38 +0200

libcommons-el-java (1.0-3) unstable; urgency=low

  * kaffe compiler transition 
  * Standards-Version 3.6.2 (no changes)
  * Bumped debhelper version to fix linda error
  * Added myself to uploaders

 -- Wolfgang Baer <WBaer@gmx.de>  Sun, 15 Jan 2006 17:22:06 +0100

libcommons-el-java (1.0-2) unstable; urgency=low

  * libant1.6-java to ant transition

 -- Arnaud Vandyck <avdyk@debian.org>  Sat, 20 Aug 2005 19:48:07 +0200

libcommons-el-java (1.0-1) unstable; urgency=low

  * Initial Release.

 -- Arnaud Vandyck <arnaud.vandyck@ulg.ac.be>  Tue,  1 Jul 2003 17:04:27 +0200

